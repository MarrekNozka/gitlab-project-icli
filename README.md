GitLab project interactive CLI
=======================================

* Create a new GitLab project
* Delete a old GitLab project :)

Dependences
----------------

* [python-gitlab](https://python-gitlab.readthedocs.io/en/stable/index.html)
* [Click](https://click.palletsprojects.com)
* [fzy](https://github.com/jhawthorn/fzy)

Install 
------------------

```
pip install git+https://gitlab.com/MarrekNozka/gitlab-project-icli
```


Usage
-----------

`glab-icreate`
```
Usage: create [OPTIONS] [PROJECT_NAME]...

  Create project on GilLab. If PROJECT_NAME is not specified, it will use
  current directory name. If PROJECT_NAME is multiple, it will create one
  project which name is joining multiple names by space.

Options:
  -p, --path TEXT             Path of project. If is'n specified, it will use
                              current directory name. This option excludes -a
                              option.
  -a, --path-autogen          If set path is generating automatically by
                              PROJECT_NAME. This optin excludes -p opion.
  -c, --config-file FILENAME  Path of config file.
  -s, --server TEXT           Gitlab server. Default is 'global.default'
                              option from ~/.python-gitlab.cfg
  --ssh                       Use SSH as connection method. This owerride
                              [server].default_method option from CONFIG-FILE.
  --https                     Use HTTPS as connection method. This owerride
                              [server].default_method option from CONFIG-FILE.
  --help                      Show this message and exit.
```

`glab-idelete`
```
Usage: delete [OPTIONS] [PROJECT_NAME]...

Options:
  -c, --config-file FILENAME  Path of config file.
  -y, --yes                   Default answer for project delete confirmation
                              is YES.
  --help                      Show this message and exit.
```


![https://github.com/nbedos/termtosvg](icli.svg)
