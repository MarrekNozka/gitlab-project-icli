import configparser
import click


def read_config(config_file, server=None):
    config = configparser.ConfigParser()
    config.read_file(config_file)

    try:
        if server is None:
            server = config["global"]["default"]
        gitlab_url = config[server]["url"]
        gitlab_token = config[server]["private_token"]
        default_method = config[server].get("default_method", "ssh")

        return gitlab_url, gitlab_token, default_method
    except KeyError as e:
        click.secho("\nOops, config file is not valid. Mayby? :)", fg="red", bold=True)
        click.echo(f"I'm expecting key {e} in config file {config_file.name}")
        click.echo(
            "See https://python-gitlab.readthedocs.io/en/stable/cli-usage.html#configuration-file-format"
        )
        raise click.Abort()
