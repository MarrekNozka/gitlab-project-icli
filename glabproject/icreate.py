#!/usr/bin/env pypy

import gitlab
from collections import OrderedDict
from subprocess import run
import click
import os
import re
from .config import read_config
from datetime import date, timedelta


@click.command()
@click.option(
    "-p",
    "--path",
    "path",
    type=str,
    default=os.path.basename(os.getcwd()),
    help="Path of project. If is'n specified, it will use current directory name. This option excludes -a option.",
    flag_value=os.path.basename(os.getcwd()),
    is_flag=False,
)
@click.option(
    "-a",
    "--path-autogen",
    "path",
    type=str,
    help="If set path is generating automatically by PROJECT_NAME. This optin excludes -p opion.",
    is_flag=True,
    flag_value="__automatically-generated__",
)
@click.option(
    "-c",
    "--config-file",
    type=click.File("r"),
    help="Path of config file.",
    default=os.path.expanduser("~/.python-gitlab.cfg"),
)
@click.option(
    "-s",
    "--server",
    type=str,
    help="Gitlab server. Default is 'global.default' option from ~/.python-gitlab.cfg",
)
@click.option(
    "--ssh",
    "connect_method",
    flag_value="ssh",
    help="Use SSH as connection method. This owerride [server].default_method option from CONFIG-FILE.",
)
@click.option(
    "--https",
    "connect_method",
    flag_value="https",
    help="Use HTTPS as connection method. This owerride [server].default_method option from CONFIG-FILE.",
)
@click.argument("project_name", nargs=-1, required=False)
def create(project_name, path, config_file, server, connect_method):
    """Create project on GilLab.
    If PROJECT_NAME is not specified, it will use current directory name.
    If PROJECT_NAME is multiple, it will create one project
    which name is joining multiple names by space.

    Config file format see https://python-gitlab.readthedocs.io/en/stable/cli-usage.html#configuration-file-format
    """

    g_url, g_token, default_method = read_config(config_file, server=server)

    with gitlab.Gitlab(g_url, g_token) as glab:
        try:
            glab.auth()
            user = glab.user

            groups = OrderedDict()
            groups[f"{user.name:42}/{user.username}"] = user
            gr = glab.groups.list()
            for group in sorted(gr, key=lambda g: g.full_path):
                groups[f"{group.name:42}/{group.full_path}"] = group

            if project_name:
                project_name = " ".join(project_name)
            else:
                project_name = os.path.basename(os.getcwd())

            click.echo(f"Create project >{project_name}< in/on:")

            # choice Group
            choices = "\n".join(groups.keys())
            fzy = run(
                ["fzy"],
                input=choices.encode("utf8"),
                capture_output=True,
            )
            group = fzy.stdout.decode("utf8").strip()
            if not group:
                raise click.Abort()

            # choice visibility
            fzy = run(
                ["fzy"],
                input="public\nprivate\ninternal".encode("utf8"),
                capture_output=True,
            )
            visibility = fzy.stdout.decode("utf8").strip()
            if not visibility:
                raise click.Abort()

            create_options = {"name": project_name, "visibility": visibility}
            if "Group" in str(type(groups[group])):
                create_options["namespace_id"] = groups[group].id
            if path != "__automatically-generated__":
                create_options["path"] = path
            # click.echo(path)
            # click.echo(create_options)
            # raise click.Abort()
            project = glab.projects.create(create_options)

            click.echo(f"Project >{project.name}< created with id {project.id}")
            click.echo(f"         {'-'*len(project.name)}")
            click.echo(f"Project url: {project.web_url}")
            click.echo(f"Project ssh url: {project.ssh_url_to_repo}")
            click.echo(f"Project http url: {project.http_url_to_repo}")
            click.echo(f"Project gitlab url: {project.web_url}")

            click.echo("\n>>> git status || git init --initial-branch=main")
            run("git status || git init", shell=True)
            origin_name = click.prompt("\nEnter origin name", default="origin")
            method = connect_method if connect_method else default_method
            if method == "ssh":
                origin_url = project.ssh_url_to_repo
            else:
                access_token = project.access_tokens.create(
                    {
                        "name": "icreate",
                        "scopes": ["api"],
                        "expires_at": (date.today() + timedelta(days=365)).isoformat(),
                    }
                )
                origin_url = project.http_url_to_repo
                origin_url = re.sub(
                    "https://",
                    f"https://{user.username}:{access_token.token}",
                    origin_url,
                )
            click.echo(f"\n>>> git remote add {origin_name} {origin_url}")
            git_remote = run(
                ["git", "remote", "add", origin_name, origin_url], capture_output=True
            )
            if git_remote.returncode != 0:
                click.secho(git_remote.stderr.decode("utf8"), fg="red", bold=True)
            else:
                if git_remote.stdout.decode("utf8").strip():
                    click.echo(git_remote.stdout.decode("utf8"))
                click.secho("OK", fg="green", bold=True)
            click.secho("\nMaybe you want type: ...", bold=True)
            click.echo(
                """git add .
git commit -m Init
git push -u origin main

"""
            )

        except gitlab.exceptions.GitlabAuthenticationError:
            click.secho(
                "\nOops, something is wrong with your credentials.", fg="red", bold=True
            )
            raise click.Abort()
        except gitlab.exceptions.GitlabCreateError as e:
            click.secho(
                "\nOops, something is wrong with project cration.", fg="red", bold=True
            )
            click.echo(e.error_message)
            raise click.Abort()
        except gitlab.exceptions.GitlabHttpError as e:
            click.secho("\nOops, something is wrong with HTTP.", fg="red", bold=True)
            click.echo(e.error_message)
            raise click.Abort()
        except gitlab.exceptions.GitlabError as e:
            click.secho("\nOops, something is wrong...", fg="red", bold=True)
            click.echo(e.error_message)
            raise click.Abort()
        except FileNotFoundError as e:
            click.secho("File not found: " + e.filename, bold=True)
            click.secho(
                "\nOops, this program dependence on FZY program.", fg="red", bold=True
            )
            click.echo("https://github.com/jhawthorn/fzy")
            click.echo("run >sudo apt install fzy< or similar")
            raise click.Abort()
