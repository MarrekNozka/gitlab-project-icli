#!/usr/bin/env pypy

import gitlab
from gitlab import Gitlab
from collections import OrderedDict
from subprocess import run
import click
import os
from .config import read_config


@click.command()
@click.option(
    "-c",
    "--config-file",
    type=click.File("r"),
    help="Path of config file.",
    default=os.path.expanduser("~/.python-gitlab.cfg"),
)
@click.option(
    "-y",
    "--yes",
    is_flag=True,
    default=False,
    help="Default answer for project delete confirmation is YES.",
)
@click.argument("project_name", nargs=-1, required=False)
def delete(project_name, config_file, yes):
    """
    Config file format see https://python-gitlab.readthedocs.io/en/stable/cli-usage.html#configuration-file-format

    """

    g_url, g_token, default_method = read_config(config_file)
    with Gitlab(g_url, g_token) as glab:
        try:
            glab.auth()
            # user = glab.user

            _projects_ = glab.projects.list(
                owned=True, order_by="name", sort="asc", get_all=True
            )

            if project_name:
                projects_for_delete = list(project_name)
                for project in _projects_:
                    if project.name in project_name:
                        bname = click.style(f"{project.name}", bold=True)
                        bpath = click.style(f"{project.path_with_namespace}", bold=True)
                        if click.confirm(
                            f"Realy delete >{bname}< project on {bpath}? ", default=yes
                        ):
                            glab.projects.delete(id=project.id)
                            click.echo(f"Deleted >{bname}< project!")
                        projects_for_delete.remove(project.name)
                for project in projects_for_delete:
                    click.echo(f"Project >{project}< not found.")
                exit(0)
            # else

            projects = OrderedDict()
            for project in _projects_:
                projects[f"{project.name:42}/{project.path_with_namespace}"] = project

            # choice Group
            choices = "\n".join(projects.keys())
            fzy = (
                run(
                    ["fzy"],
                    input=choices.encode("utf8"),
                    capture_output=True,
                )
                .stdout.decode("utf8")
                .strip()
            )
            if not fzy:
                raise click.Abort()
            click.echo(fzy + "\n")
            bname = click.style(f"{projects[fzy].name}", bold=True)
            if click.confirm(
                f"Realy delete >{bname}< project?",
                default=yes,
            ):
                glab.projects.delete(id=projects[fzy].id)
                click.echo(f"Deleted >{bname}< project!")

        except gitlab.exceptions.GitlabAuthenticationError:
            click.secho(
                "\nOops, something is wrong with your credentials.", fg="red", bold=True
            )
            raise click.Abort()
        except gitlab.exceptions.GitlabDeleteError as e:
            click.secho(
                "\nOops, something is wrong with project cration.", fg="red", bold=True
            )
            click.echo(e.error_message)
            raise click.Abort()
        except gitlab.exceptions.GitlabHttpError as e:
            click.secho("\nOops, something is wrong with HTTP.", fg="red", bold=True)
            click.echo(e.error_message)
            raise click.Abort()
        except gitlab.exceptions.GitlabError as e:
            click.secho("\nOops, something is wrong...", fg="red", bold=True)
            click.echo(e.error_message)
            raise click.Abort()
